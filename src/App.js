import './App.css';
import Web3 from 'web3';
import { useState } from 'react';
var Tx = require('ethereumjs-tx');

const web3 = new Web3(Web3.givenProvider || "http://localhost:8545");
// private key ownera vseh coinov, katerega ob pravi pogodbi moramo dosti bolje skrit kot zdaj
const my_privkey = "a476dec80d995e6b810528075d96f96d0c1566e84b65707954f59b44aeb2f8d6"
var contractAddress = "0x166f40290b3c62d1c91a89c7eeec7a39ee105167";


// je template funkcij ki jih contract vsebuje. Napises samo tiste ki jih rabis, torej v tem primeru balance of pa transfer
let minABI = [
  // balanceOf
  {
    "constant": true,
    "inputs": [{ "name": "_owner", "type": "address" }],
    "name": "balanceOf",
    "outputs": [{ "name": "balance", "type": "uint256" }],
    "type": "function"
  },
  //transfer
  {
    "constant": false,
    "inputs": [
      {
        "name": "_to",
        "type": "address"
      },
      {
        "name": "_value",
        "type": "uint256"
      }
    ],
    "name": "transfer",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  }
];

// prenos iz enega na drugi racun
let transfere = async (myAddress, destAddress, transferAmount) => {
  await window.ethereum.enable();
  var count = await web3.eth.getTransactionCount(myAddress);
  var contract = new web3.eth.Contract(minABI, contractAddress, { from: myAddress });
  var balance = await contract.methods.balanceOf(myAddress).call();

  // trenutno na testnem omrezju se gas price in limit fixno doloca, kasneje se bo to dinamicno spreminjalo
  // pomemben je tudi nonce ki more bit obvezno stevilo transakcij tega addressa
  var rawTransaction = {
    "from": myAddress,
    "nonce": "0x" + count.toString(16),
    "gasPrice": "0x6FC23AC00",
    "gasLimit": "0x250CA",
    "to": contractAddress,
    "value": "0x0",
    "data": contract.methods.transfer(destAddress, transferAmount).encodeABI(),
    "chainId": 0x61
  };

  // zakodira se se private key, katerega ob pravi pogodbi moramo dosti bolje skrit kot zdaj
  var privKey = new Buffer(my_privkey, 'hex');
  var tx = new Tx(rawTransaction);
  tx.sign(privKey);
  var serializedTx = tx.serialize();

  // posljemo transakcijo, receipt ima vse podatke, o transaction hashing in ostalo, ponavadi se to izpise v smislu "track your transaction here"
  let receipt = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
  console.log(`Racun:  ${JSON.stringify(receipt, null, '\t')}`);
}

const ownerAddress = "0xd0261CECaFA798884f9f57E3678E3dB13C5AF2E6"
function App() {
  const [accName, setAccName] = useState("")
  const [transactionMessage, setTransactionMessage] = useState("")
  const [tokens, setTokens] = useState(0)
  const [error, setError] = useState(false)

  var submitClicked = async () => {
    setTransactionMessage("Transaction in progress")
    try {
      await transfere(ownerAddress, accName, 200)
      await setBalance(accName)
      setTransactionMessage("Transaction successful")
    } catch (e) {
      setTransactionMessage("Errro during transaction")
    }
  }

  let setBalance = async (account) => {
    let contract = new web3.eth.Contract(minABI, contractAddress);
    let balance = await contract.methods.balanceOf(account).call();
    setTokens(balance)
  }

  // CONNECTANJE WALLETA/ACCOUNTA
  var connectWallet = async () => {
    await window.ethereum.enable();
    let acc = await web3.eth.getAccounts()
    if (acc.length > 0) {
      console.log(acc)
      setAccName(acc[0])
      try {
        await setBalance(acc[0])
        setError(false)
      } catch (e) {
        setError(true)
      }
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h4>Hello please connect your wallet. BSC testnet only.</h4>
        <button onClick={connectWallet}>Connect</button>
        {accName && <p>{accName}</p>}
        {accName && <p>Tokens count: {tokens}</p>}
        {error && <h1>INCORRECT NETWORK, PLEASE CHANGE TO BSC TESTNET</h1>}
        {accName && !error && <div>
          <p>This is dev tools for contract holder</p>
          <p>
            target wallet address
          </p>
          <input type="text" name="name" value={accName} />
          <button onClick={submitClicked}>Send</button>
          <p>{transactionMessage}</p>
        </div>
       }
      </header>
    </div>
  );
}

export default App;
